# CodeReaderNg6LibApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.1.2.

## Installation

Add library to `package.json` via bash:

```
npm install https://bitbucket.org/skubal_dudr_cz/code-reader-ng6-lib/downloads/code-reader-ng6-lib-0.0.2.tgz --save
```

Then add code to template. For example:

```html
<dudr-code-reader (loadRfidEvent)="receiveRfid($event)" (loadQrEvent)="receiveQr($event)"></dudr-code-reader>
```

Function callbacks in `loadRfidEvent` and `loadQrEvent` are fired whenever code of its type is detected.

## How it works

Service captures keystrokes.

To distinguish between RFID or QR code detection, service assumes QR codes contains `Minus` char.

## Library deployment:

Run in bash:

```
npm run package
```

## Error workarounds

### Typing certain keys leads to infinite loop of event firings

Make sure the component is initiated only once. If you need capturing from several components, it should be used in one place (typically root component) and should be listened via singleton service.

## TODO:

- ignore selected inputs
- show state of key listening
