import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CodeReaderNg6LibModule } from 'code-reader-ng6-lib';

import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CodeReaderNg6LibModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
