import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'code-reader-ng6-lib-app';
  code: string;
  type: string;
  isLoaded = false;
  receiveRfidCode($event) {
    this.code = $event;
    this.type = 'RFID';
    this.isLoaded = true;
  }
  receiveQrCode($event) {
    this.code = $event;
    this.type = 'QR';
    this.isLoaded = true;
  }
}
