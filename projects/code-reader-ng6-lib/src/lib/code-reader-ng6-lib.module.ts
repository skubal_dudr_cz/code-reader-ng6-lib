import { NgModule } from '@angular/core';
import { CodeReaderNg6LibComponent } from './code-reader-ng6-lib.component';
import { CodeReaderComponent } from './code-reader/code-reader.component';

@NgModule({
  imports: [
  ],
  declarations: [
    CodeReaderNg6LibComponent,
    CodeReaderComponent
  ],
  exports: [
    CodeReaderNg6LibComponent,
    CodeReaderComponent
  ]
})
export class CodeReaderNg6LibModule { }
