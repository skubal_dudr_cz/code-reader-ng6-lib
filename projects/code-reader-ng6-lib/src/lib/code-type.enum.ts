export enum CodeType {
  Rfid = 'rfid',
  Qr = 'qr'
}
