import { TestBed, inject } from '@angular/core/testing';

import { CodeReaderNg6LibService } from './code-reader-ng6-lib.service';

describe('CodeReaderNg6LibService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CodeReaderNg6LibService]
    });
  });

  it('should be created', inject([CodeReaderNg6LibService], (service: CodeReaderNg6LibService) => {
    expect(service).toBeTruthy();
  }));
});
