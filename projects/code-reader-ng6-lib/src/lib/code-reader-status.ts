import { CodeType } from './code-type.enum';

export interface CodeReaderStatus {
  code: string;
  type: CodeType;
}
