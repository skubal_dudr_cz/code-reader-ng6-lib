import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CodeReaderNg6LibComponent } from './code-reader-ng6-lib.component';

describe('CodeReaderNg6LibComponent', () => {
  let component: CodeReaderNg6LibComponent;
  let fixture: ComponentFixture<CodeReaderNg6LibComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CodeReaderNg6LibComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeReaderNg6LibComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
